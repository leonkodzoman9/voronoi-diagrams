#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <random>
#include <numeric>
#include <numbers>

#include <array>
#include <vector>
#include <span>

#include <immintrin.h>

