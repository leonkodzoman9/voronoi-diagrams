#include "Includes.h"

#include "Keyboard.h"
#include "Timer.h"



static constexpr int2 Resolution = { 1920, 1080 };
static constexpr int PointCount = 64;



int main() {

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	GLFWwindow* window = glfwCreateWindow(Resolution.x, Resolution.y, "Voronoi", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(0);

	Keyboard& keyboard = Keyboard::getInstance();






	std::mt19937 generator(0);
	std::uniform_real_distribution<> distribution(0, 1);

	std::vector<float2> points(PointCount);
	std::vector<float2> velocities(PointCount);
	std::vector<uchar4> colors(PointCount);
	for (int i = 0; i < PointCount; i++) {
		points[i].x = distribution(generator) * (Resolution.x - 10) + 5;
		points[i].y = distribution(generator) * (Resolution.y - 10) + 5;

		velocities[i].x = distribution(generator) * 2 - 1;
		velocities[i].y = distribution(generator) * 2 - 1;

		colors[i].x = distribution(generator) * 255.999999;
		colors[i].y = distribution(generator) * 255.999999;
		colors[i].z = distribution(generator) * 255.999999;
		colors[i].w = 255;
	}



	int threadCount = 1;
	std::vector<uchar4> framebuffer(Resolution.x * Resolution.y);

	MultiTimer frametimeTimer(100);
	MultiTimer executionTimer(100);
	while (!glfwWindowShouldClose(window)) {

		frametimeTimer.update();

		std::string frametime = std::to_string(frametimeTimer.getAverageInterval()) + " ms";
		std::string execution = std::to_string(executionTimer.getAverageInterval()) + " ms";

		glfwSetWindowTitle(window, (frametime + ", " + execution).data());

		glfwPollEvents();

		keyboard.update(window);

		glClear(GL_COLOR_BUFFER_BIT);

		

		executionTimer.stop();

		/*for (int i = 0; i < Resolution.y; i++) {
			for (int j = 0; j < Resolution.x; j++) {
				
				int nearestIndex = 0;
				float nearestDistance = FLT_MAX;
				for (int k = 0; k < PointCount; k++) {
					float dx = points[k].x - j;
					float dy = points[k].y - i;
					float distance = dx * dx + dy * dy;
					if (distance < nearestDistance) {
						nearestDistance = distance;
						nearestIndex = k;
					}
				}

				framebuffer[i * Resolution.x + j] = colors[nearestIndex];
			}
		}*/

		__m256 _f32_1 = _mm256_set1_ps(1);
		__m256 _f32_8 = _mm256_set1_ps(8);
		__m256 _f32_FLT_MAX = _mm256_set1_ps(FLT_MAX);
		__m256 _f32_01234567 = _mm256_setr_ps(0, 1, 2, 3, 4, 5, 6, 7);
		__m256i _i32_1 = _mm256_set1_epi32(1);

		__m256 _i = _mm256_setzero_ps();
		for (int i = 0; i < Resolution.y; i++) {

			__m256 _j = _f32_01234567;
			for (int j = 0; j < Resolution.x; j += 8) {

				__m256i _nearestIndex = _mm256_setzero_si256();
				__m256 _nearestDistance = _f32_FLT_MAX;

				__m256i _k = _mm256_setzero_si256();
				for (int k = 0; k < PointCount; k++) {

					__m256 _px = _mm256_set1_ps(points[k].x);
					__m256 _py = _mm256_set1_ps(points[k].y);

					__m256 _dx = _mm256_sub_ps(_px, _j);
					__m256 _dy = _mm256_sub_ps(_py, _i);
					__m256 _distance = _mm256_add_ps(_mm256_mul_ps(_dx, _dx), _mm256_mul_ps(_dy, _dy));
					
					__mmask8 _mask = _mm256_cmp_ps_mask(_distance, _nearestDistance, _CMP_LT_OQ);

					_nearestDistance = _mm256_mask_blend_ps(_mask, _nearestDistance, _distance);
					_nearestIndex = _mm256_mask_blend_epi32(_mask, _nearestIndex, _k);

					_k = _mm256_add_epi32(_k, _i32_1);
				}

				_mm256_storeu_epi32(&framebuffer[i * Resolution.x + j], _mm256_i32gather_epi32((int*)colors.data(), _nearestIndex, 4));

				_j = _mm256_add_ps(_j, _f32_8);
			}
			_i = _mm256_add_ps(_i, _f32_1);
		}

		executionTimer.update();



		for (int k = 0; k < PointCount; k++) {
			int x = points[k].x;
			int y = points[k].y;
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					framebuffer[(y + i) * Resolution.x + (x + j)] = { 0, 0, 0, 255 };
				}
			}
		}

		for (int i = 0; i < points.size(); i++) {
			points[i].x += velocities[i].x;
			points[i].y += velocities[i].y;
			if (points[i].x < 5 || points[i].x >= Resolution.x - 5) { velocities[i].x *= -1; }
			if (points[i].y < 5 || points[i].y >= Resolution.y - 5) { velocities[i].y *= -1; }
		}

		glDrawPixels(Resolution.x, Resolution.y, GL_RGBA, GL_UNSIGNED_BYTE, framebuffer.data());

		glfwSwapBuffers(window);
	}

	return 0;
}